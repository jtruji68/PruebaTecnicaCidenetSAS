package com.juandavidt.PruebaTecnica.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerErrorHandler {

@ExceptionHandler(EmpleadoException.class)
    public ResponseEntity<?> handleEmpleadoException(EmpleadoException ex){

    return ResponseEntity.status(ex.getStatus()).body(ex.getErrorMessage());
}

}
