package com.juandavidt.PruebaTecnica.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "area")
@Getter
@Setter
@NoArgsConstructor
public class Area {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "nombre_area")
    private String nombreArea;

    @Column(name = "es_admin")
    private boolean isAdmin = false;


}
