package com.juandavidt.PruebaTecnica.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "empleado")
@Getter
@Setter
@NoArgsConstructor
public class Empleado {

    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "primer_apellido",length = 20)
    private String primerApellido;

    @Column(name = "segundo_apellido",length = 20)
    private String segundoApellido;

    @Column(name = "primer_nombre",length = 20)
    private String primerNombre;

    @Column(name = "otros_nombres",length = 50)
    private String otrosNombres;

    @ManyToOne
    @JoinColumn(name = "id_pais_empleo")      //
    private Pais paisEmpleo;

    @ManyToOne
    @JoinColumn(name = "id_tipo_identificacion")     //
    private Identificacion tipoIdentificacion;

    @Column(name = "numeroIdentificacion",length = 20,unique = true)
    private String numeroIdentificacion;

    @Column(name = "correo_electronico",length = 300,unique = true)
    private String correoElectronico;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_ingreso")
    private Date fechaIngreso;

    @ManyToOne
    @JoinColumn(name = "id_area_trabajo")     //
    private Area areaTrabajo;

    @Column(name = "activo")
    private boolean activo;

    @JsonFormat(pattern="dd/MM/YYYY HH:mm:ss")
    @UpdateTimestamp
    @Column(name = "fecha_hora_registro")
    private Date fechaHoraRegistro;



}
