package com.juandavidt.PruebaTecnica.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "identificacion")
@Getter
@Setter
@NoArgsConstructor
public class Identificacion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "tipo_identificacion")
    private String tipoIdentificacion;


}
