package com.juandavidt.PruebaTecnica.rest;

import com.juandavidt.PruebaTecnica.exception.EmpleadoException;
import com.juandavidt.PruebaTecnica.model.Empleado;
import com.juandavidt.PruebaTecnica.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.*;

@RestController
@RequestMapping("/api/empleados")
public class EmpleadoRest {

    @Autowired
    private EmpleadoService empleadoService;

    @GetMapping
    private ResponseEntity<List<Empleado>> getAllEmpleados(){
        return ResponseEntity.ok(empleadoService.findAll());
    }

    @GetMapping ("/{id}")
    private ResponseEntity<Empleado> getEmpleado(@PathVariable ("id") Integer id){
        return ResponseEntity.ok(Objects.requireNonNull(empleadoService.findById(id).orElse(null)));
    }

    @PostMapping
    private ResponseEntity<Empleado> saveEmpleado(@RequestBody Empleado empleado) throws URISyntaxException {
        Empleado empleadoGuardado = empleadoService.save(empleado);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping
    private ResponseEntity<Empleado> editEmpleado(@RequestBody Empleado empleado) throws URISyntaxException {
        if(!empleadoService.existsById(empleado.getId())){
            throw new EmpleadoException(HttpStatus.BAD_REQUEST,"El id del empleado a modificar no se encuentra en la base de datos");
        }
        Empleado empleadoGuardado = empleadoService.save(empleado);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping ("delete/{id}")
    private ResponseEntity<Boolean> deleteEmpleado (@PathVariable ("id") Integer id){
        empleadoService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
