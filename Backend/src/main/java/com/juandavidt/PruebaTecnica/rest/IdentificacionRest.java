package com.juandavidt.PruebaTecnica.rest;

import com.juandavidt.PruebaTecnica.model.Identificacion;
import com.juandavidt.PruebaTecnica.service.IdentificacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/identificaciones")
public class IdentificacionRest {

    @Autowired
    private IdentificacionService identificacionService;

    @GetMapping
    private ResponseEntity<List<Identificacion>> getAllIdentificacion(){
        return ResponseEntity.ok(identificacionService.findAll());
    }
}
