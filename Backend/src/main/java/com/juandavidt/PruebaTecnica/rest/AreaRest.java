package com.juandavidt.PruebaTecnica.rest;

import com.juandavidt.PruebaTecnica.model.Area;
import com.juandavidt.PruebaTecnica.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/areas")
public class AreaRest {

    @Autowired
    private AreaService areaService;

    @GetMapping
    private ResponseEntity<List<Area>> getAllAreas(){
        return ResponseEntity.ok(areaService.findAll());
    }
}
