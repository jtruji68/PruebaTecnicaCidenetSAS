package com.juandavidt.PruebaTecnica.service;

import com.juandavidt.PruebaTecnica.model.Area;
import com.juandavidt.PruebaTecnica.repository.AreaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AreaService {

    private AreaRepository areaRepository;


    public List<Area> findAll() {
        return areaRepository.findAll();
    }


    public List<Area> findAll(Sort sort) {
        return areaRepository.findAll(sort);
    }


    public Page<Area> findAll(Pageable pageable) {
        return areaRepository.findAll(pageable);
    }


    public List<Area> findAllById(Iterable<Integer> iterable) {
        return areaRepository.findAllById(iterable);
    }

    public void deleteById(Integer integer) {
        areaRepository.deleteById(integer);
    }


    public <S extends Area> S save(S s) {
        return areaRepository.save(s);
    }

    public Optional<Area> findById(Integer integer) {
        return Optional.empty();
    }

    public boolean existsById(Integer integer) {
        return areaRepository.existsById(integer);
    }

}
