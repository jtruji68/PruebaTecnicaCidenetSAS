package com.juandavidt.PruebaTecnica.service;

import com.juandavidt.PruebaTecnica.model.Pais;
import com.juandavidt.PruebaTecnica.repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaisService {

    @Autowired
    private PaisRepository paisRepository;

    
    public List<Pais> findAll() {
        return paisRepository.findAll();
    }

    public List<Pais> findAll(Sort sort) {
        return paisRepository.findAll(sort);
    }

    public Page<Pais> findAll(Pageable pageable) {
        return paisRepository.findAll(pageable);
    }

    public List<Pais> findAllById(Iterable<Integer> iterable) {
        return paisRepository.findAllById(iterable);
    }

    public void deleteById(Integer integer) {
        paisRepository.deleteById(integer);
    }

    public <S extends Pais> S save(S s) {
        return paisRepository.save(s);
    }

    public Optional<Pais> findById(Integer integer) {
        return paisRepository.findById(integer);
    }

    public boolean existsById(Integer integer) {
        return paisRepository.existsById(integer);
    }

    public <S extends Pais> S saveAndFlush(S s) {
        return null;
    }


}
