package com.juandavidt.PruebaTecnica.service;

import com.juandavidt.PruebaTecnica.exception.EmpleadoException;
import com.juandavidt.PruebaTecnica.model.Empleado;
import com.juandavidt.PruebaTecnica.model.Pais;
import com.juandavidt.PruebaTecnica.repository.EmpleadoRepository;
import com.juandavidt.PruebaTecnica.repository.PaisRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpleadoService {

    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Autowired
    private PaisRepository paisRepository;


    public List<Empleado> findAll() {
        return empleadoRepository.findAll();
    }

    public void deleteById(Integer id) {
        if(empleadoRepository.existsById(id)){
            empleadoRepository.deleteById(id);
        }else {
            throw new EmpleadoException(HttpStatus.BAD_REQUEST,"El id del empleado a borrar no se encuentra en la base de datos");
        }
    }

    public void delete(Empleado empleado) { empleadoRepository.delete(empleado); }

    public <S extends Empleado> S save(S empleado) {

        if(!empleadoRepository.findByNumeroIdentificacion(empleado.getNumeroIdentificacion()).isEmpty()){
            throw new EmpleadoException(HttpStatus.BAD_REQUEST,"El id del empleado ya se encuentra en la base de datos");
        }
        int id= 0;
        while (true){
            String primerNombre = empleado.getPrimerNombre().toUpperCase();
            String primerApellido = empleado.getPrimerApellido().toUpperCase().replace(" ","");
            int idPais = empleado.getPaisEmpleo().getId();
            Optional<Pais> pais = paisRepository.findById(idPais);
            String dominio = pais.get().getCodigo();
            String ID;
            if (id== 0) { ID = ""; } else {ID = "."+ id;}
            String correo = primerNombre+"."+primerApellido+ID+"@cidenet.com."+ dominio;

            List <Empleado> lista = empleadoRepository.findByCorreoElectronico(correo);

            if (!lista.isEmpty()){
                if(lista.get(0).getId().equals(empleado.getId())){
                    continue;
                }
                id++;
                continue;
            }
            empleado.setCorreoElectronico(correo);
            break;
        }

        return empleadoRepository.save(empleado);
    }

    public Optional<Empleado> findById(Integer integer) {
        if(empleadoRepository.existsById(integer)){
            return empleadoRepository.findById(integer);
        }else{
            throw new EmpleadoException(HttpStatus.BAD_REQUEST,"No se encontro el empleado");
        }
    }

    public boolean existsById(Integer integer) {
        return empleadoRepository.existsById(integer);
    }

    public List<Empleado> findByCorreoElectronico(String correo) {
        return empleadoRepository.findByCorreoElectronico(correo);
    }

    public List<Empleado> findByNumeroIdentificacion(String identificacion) {
        return empleadoRepository.findByNumeroIdentificacion(identificacion);
    }
}
