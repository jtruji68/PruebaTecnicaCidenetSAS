package com.juandavidt.PruebaTecnica.service;

import com.juandavidt.PruebaTecnica.model.Identificacion;
import com.juandavidt.PruebaTecnica.repository.IdentificacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class IdentificacionService {

    @Autowired
    private IdentificacionRepository identificacionRepository;

    public List<Identificacion> findAll() {
        return identificacionRepository.findAll();
    }

    public List<Identificacion> findAllById(Iterable<Integer> iterable) {
        return identificacionRepository.findAllById(iterable);
    }
    public void deleteById(Integer integer) {
        identificacionRepository.deleteById(integer);
    }

    public void delete(Identificacion identificacion) {
        identificacionRepository.delete(identificacion);
    }

    public <S extends Identificacion> S save(S s) {
        return identificacionRepository.save(s);
    }

    public boolean existsById(Integer integer) {
        return identificacionRepository.existsById(integer);
    }



}
