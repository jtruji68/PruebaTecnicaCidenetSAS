package com.juandavidt.PruebaTecnica.repository;


import com.juandavidt.PruebaTecnica.model.Area;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaRepository extends JpaRepository<Area,Integer> {
}
