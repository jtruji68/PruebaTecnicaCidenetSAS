package com.juandavidt.PruebaTecnica.repository;
import com.juandavidt.PruebaTecnica.model.Pais;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<Pais,Integer> {
}
