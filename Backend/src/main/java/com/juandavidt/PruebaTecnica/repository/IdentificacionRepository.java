package com.juandavidt.PruebaTecnica.repository;

import com.juandavidt.PruebaTecnica.model.Identificacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentificacionRepository extends JpaRepository<Identificacion,Integer> {
}
