package com.juandavidt.PruebaTecnica.repository;


import com.juandavidt.PruebaTecnica.model.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface EmpleadoRepository extends JpaRepository<Empleado,Integer> {

    List<Empleado> findByCorreoElectronico(String correo);
    List<Empleado> findByNumeroIdentificacion(String identificacion);

}
