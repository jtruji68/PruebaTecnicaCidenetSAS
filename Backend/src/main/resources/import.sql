INSERT INTO PAIS (ID,CODIGO,NOMBRE_PAIS) VALUES (1,'co','Colombia'),(2,'us','Estados Unidos');
INSERT INTO AREA (ID,ES_ADMIN,NOMBRE_AREA) VALUES (1,FALSE,'Administración'),(2,FALSE,'Financiera'),(3,FALSE,'Compras'),(4,FALSE,'Infraestructura'),(5,FALSE,'Operación'),(6,TRUE,'Talento Humano'),(7,FALSE,'Servicios Varios'),(8,FALSE,'Operación');
INSERT INTO IDENTIFICACION (ID,TIPO_IDENTIFICACION) VALUES (1,'Cédula de Ciudadanía'),(2,'Cédula de Extranjería'),(3,'Pasaporte'),(4,'Permiso Especial');
