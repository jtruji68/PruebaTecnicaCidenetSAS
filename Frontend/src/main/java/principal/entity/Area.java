package principal.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Area {

    private int id;
    private String nombreArea;
    private boolean isAdmin;

}
