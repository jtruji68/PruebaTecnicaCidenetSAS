package principal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Pais {

    private int id;
    private String nombrePais;
    private String codigo;
}
