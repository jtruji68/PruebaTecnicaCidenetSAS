package principal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Identificacion {

    private int id;
    private String tipoIdentificacion;
}
