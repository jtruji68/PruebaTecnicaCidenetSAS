package principal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
public class Empleado {

    private Integer id;
    private String primerApellido;
    private String segundoApellido;
    private String primerNombre;
    private String otrosNombres;
    private int paisEmpleo;
    private int tipoIdentificacion;
    private String numeroIdentificacion;
    private String correoElectronico;
    private Date fechaIngreso;
    private int areaTrabajo;
    private boolean activo;
    private Date fechaHoraRegistro;


}
