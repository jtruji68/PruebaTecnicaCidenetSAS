package principal.dao;

import com.google.gson.Gson;
import principal.entity.Empleado;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Date;

public class EmpleadoDAO {
    Connection connection = new Connection();
    Parser parser = new Parser();
    Gson g = new Gson();


    public void getEmpleados() throws IOException {

        HttpURLConnection response = connection.connection("empleados","GET");


        try{
            if (response.getResponseCode()>=200){
                String jsonResponse = parser.jsonResponse(response);
                System.out.println(jsonResponse);




            }
        }catch (Exception e){
            System.out.println("error en la respuesta");
            e.printStackTrace();
        }

    }

    public void getEmpleado(int id) throws IOException {

        HttpURLConnection response = connection.connection("empleados/"+id,"GET");


        try{
            if (response.getResponseCode()>=200){
                String jsonResponse = parser.jsonResponse(response);
                System.out.println(jsonResponse);

                Date date = new Date();

                Empleado empleado = new Empleado(2,"Gonzales","Peralta",
                        "Gabriela","Hidalid",1,1,"124343r",
                                " ",date,1,true,date);


            }
        }catch (Exception e){
            System.out.println("error en la respuesta");
            e.printStackTrace();
        }

    }


}
