package principal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class Parser {



    public String jsonResponse(HttpURLConnection response) throws IOException {

        StringBuffer responseContent =new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getInputStream()));
        String line;
        try{
            while ((line= reader.readLine())!=null){
                responseContent.append(line);
            }
            reader.close();

            return responseContent.toString();
        }catch (Exception e){

        return "null";
        }
    }


    }
