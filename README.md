# PruebaTecnicaCidenetSAS

Prueba técnica para Programador Java Cidenet SAS. Conformada por un backend en Spring Framework, y front realizado en Swing, utlizando H2 como gestor de base de datos. Consiste en una aplicación java de escritorio para registro de los empleados de una compañía.


## Esquema de base de datos:


![Esquema de base de datos](https://gitlab.com/jtruji68/PruebaTecnicaCidenetSAS/raw/master/db_squema.jpg)


## Ejecución

Para correr la aplicación ejecutar el archivo java:

```bash
$ Backend/PruebaTecnicaCidenetSAS/Backend/src/main/java/com/juandavidt/PruebaTecnica/PruebaTecnicaApplication.java
```

Cuando se ejecute la aplicación por primera vez el archivo import.sql ingresará los registros tales como países, áreas de trabajo y tipo de documento de identificación. Los cuales serán necesarios para poder brindar opciones en dichos campos para poder ingresar los empleados sin inconvenientes

```bash
$ Backend/PruebaTecnicaCidenetSAS/Backend/src/main/resources/import.sql
```

## API rest 
Para realizar las operaciónes CRUD se ha empleado una API rest con los siguientes métodos, tener en cuenta que al ejecutarse la aplicación se ingresan automáticamente los valores correspondientes a las tablas referentes al paisEmpleo, areaTrabajo y tipoIdentificacion:

### 1. Países 

- Para ver los países disponibles junto con sus codigos ISO, con los que se genera el correoElectronico, si es exitoso el status será 200.

- GET Request

http://localhost:8080/api/paises

<code>
[
    {
        "id": 1,
        "nombrePais": "Colombia",
        "codigo": "co"
    },
    {
        "id": 2,
        "nombrePais": "Estados Unidos",
        "codigo": "us"
    }
]

</code>

### 2. Áreas de trabajo 

- Entrega la lista de las áreas de trabajo disponibles. Si es exitoso el status será 200.

- GET Request

http://localhost:8080/api/areas


### 3. Tipos de identificación

- Entrega la lista de las áreas de trabajo disponibles. Si es exitoso el status será 200.

- GET Request

http://localhost:8080/api/api/identificaciones


### 4. Empleados

* Ingresar nuevo empleado:

- POST Request

http://localhost:8080/api/empleados

El body del request será el siguiente, si es exitoso el status será 201:

<code>

{
    "primerApellido": "Ortiz",
    "segundoApellido": "Peralta",
    "primerNombre": "Juan",
    "otrosNombres": "David",
    "paisEmpleo": {"id":1},
    "tipoIdentificacion": {"id":1},
    "numeroIdentificacion": "1235454",
    "fechaIngreso": "2019-02-03",
    "areaTrabajo":{"id":1},
    "activo": true
    
}

</code>

Donde el numero del id en los campos paisEmpleo, tipoIdentificación y areaTrabajo correponden a las ids de sus respectivos registros.


- GET Request:

* Retorna la lista de los empleados en formato Json, si es exitoso, el status será 200.

http://localhost:8080/api/empleados

La respuesta se entrega con este formato:
<code>
[
{
    "id": 1,
    "primerApellido": "Ortiz",
    "segundoApellido": "Peralta",
    "primerNombre": "Juan",
    "otrosNombres": "David",
    "paisEmpleo": {
        "id": 1,
        "nombrePais": "Colombia",
        "codigo": "co"
    },
    "tipoIdentificacion": {
        "id": 1,
        "tipoIdentificacion": "Cédula de Ciudadanía"
    },
    "numeroIdentificacion": "1235454",
    "correoElectronico": "JUAN.ORTIZ@cidenet.com.co",
    "fechaIngreso": "2019-02-02",
    "areaTrabajo": {
        "id": 1,
        "nombreArea": "Administración",
        "admin": false
    },
    "activo": true,
    "fechaHoraRegistro": "12/12/2020 02:57:45"
}
]
</code>

* Retorna la información de un empleado por id en formato Json, si es exitoso, el status será 200.

- GET Request

http://localhost:8080/api/empleados/id


<code>

{
    "id": 1,
    "primerApellido": "Ortiz",
    "segundoApellido": "Peralta",
    "primerNombre": "Juan",
    "otrosNombres": "David",
    "paisEmpleo": {
        "id": 1,
        "nombrePais": "Colombia",
        "codigo": "co"
    },
    "tipoIdentificacion": {
        "id": 1,
        "tipoIdentificacion": "Cédula de Ciudadanía"
    },
    "numeroIdentificacion": "1235454",
    "correoElectronico": "JUAN.ORTIZ@cidenet.com.co",
    "fechaIngreso": "2019-02-02",
    "areaTrabajo": {
        "id": 1,
        "nombreArea": "Administración",
        "admin": false
    },
    "activo": true,
    "fechaHoraRegistro": "12/12/2020 02:57:45"
}

</code>

donde id se reemplaza por el numero de identificación del usuario, en este caso 1.


- Eliminar empleado:

- DELETE Request
http://localhost:8080/api/empleados/delete/id

donde id se reemplaza por el numero de identificación del usuario, en este caso 1. Si es exitoso el status será 200.


- Actualizar información del empleado.

- PUT Request
http://localhost:8080/api/empleados

Notese que el id del empleado a actualizar se tiene que ingresar en el body json, mas no en la dirección http. Si es exitoso el status será ok.
<code>
{
    "id":1,
    "primerApellido": "Ortiz",
    "segundoApellido": "Peralta",
    "primerNombre": "Juan",
    "otrosNombres": "David",
    "paisEmpleo": {"id":1},
    "tipoIdentificacion": {"id":1},
    "numeroIdentificacion": "1235454",
    "fechaIngreso": "2019-02-03",
    "areaTrabajo":{"id":1},
    "activo": true
    
}

</code>


## FrontEnd

- Para el front se va a utilizar Java swing. Este se encuentra ubicado en el directorio, y se consumira la api por medio de http requests.

```bash
$ FrontEnd
```






```bash
```